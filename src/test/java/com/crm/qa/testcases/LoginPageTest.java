package com.crm.qa.testcases;

import java.io.IOException;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.*;
import com.crm.qa.base.TestBase;
import com.crm.qa.pages.HomePage;
import com.crm.qa.pages.Login;
import com.crm.qa.utilities.Utilities;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.LogStatus;

public class LoginPageTest extends TestBase{
	public Login loginPage;
	public HomePage homePage;

	LoginPageTest(){
		super();
	}
	
	@BeforeMethod
	public void setup() {
		intialization();
		loginPage = new Login();
	}

	@Test (priority =1)
	public void validateLoginPageTitleTest() {
		extentTest = extent.startTest("validateLoginPageTitleTest"); // this is for extent report
		String title = driver.getTitle();
		Assert.assertEquals(title, "CRM","The title is incorrect");
		System.out.println("title is " + title);
	}

	@Test (priority =2)
	public void login() {
		extentTest = extent.startTest("login"); // this is for extent report
		homePage=loginPage.enterCredentials(prop.getProperty("username"),prop.getProperty("password"));
	}
}
