package com.crm.qa.testcases;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.crm.qa.base.TestBase;
import com.crm.qa.pages.ContactsPage;
import com.crm.qa.pages.HomePage;
import com.crm.qa.pages.Login;
import com.crm.qa.utilities.Utilities;

public class ContactsPageTest extends TestBase{
	public Login loginPage;
	public HomePage homePage;
	public ContactsPage contactsPage;
	public Utilities utils;

	public ContactsPageTest() {
		super();
	}

	@BeforeMethod
	public void setup() {
		intialization();
		loginPage = new Login();
		utils= new Utilities();
		homePage =new HomePage();
		homePage=loginPage.enterCredentials(prop.getProperty("username"),prop.getProperty("password"));
		homePage.clickOnDeals();
		contactsPage=homePage.clickOnContacts();
	}
	
	@DataProvider
	public Object [][] getCRMTestData() {
		Object data[][]= Utilities.getTestData("Contacts");
		return data;
	}
	
	@Test(priority=2, dataProvider="getCRMTestData")
	public void createNewContactTest(String firstName,String lastName,String company,String email ) throws InterruptedException {
		contactsPage.createNewContact(firstName, lastName, company, email);
		extentTest = extent.startTest("createNewContactTest");
	//	Thread.sleep(3000);	
		homePage.clickOnContacts();
	}
}
