package com.crm.qa.base;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

import com.crm.qa.utilities.Utilities;
import com.crm.qa.utilities.WebEventListener;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class TestBase {

	public static WebDriver driver;
	public static Properties prop;
	public static EventFiringWebDriver e_driver;
	public static WebEventListener eventListener;
	public ExtentReports extent;
	public ExtentTest extentTest;

	public TestBase() {
		prop = new Properties();
		try {
			FileInputStream ip = new FileInputStream(System.getProperty("user.dir")+
					"/src/main/java/com/crm/qa/config/config.properties");
			prop.load(ip);
		} catch (FileNotFoundException e) {		
			e.printStackTrace();
		} catch (IOException e) {		
			e.printStackTrace();
		}
	}
	
	// The below @beforetest and @aftertest are for extentReportFailedTestCasesScreenshot	
	
	@BeforeTest
	public void setExtent(){
		extent = new ExtentReports(System.getProperty("user.dir")+"/test-output/ExtentReport.html", true);
		extent.addSystemInfo("Host Name", "Priyanka Mac");
		extent.addSystemInfo("User Name", "Nothing");
		extent.addSystemInfo("Environment", "QA");	
	}

	@AfterTest
	public void endReport(){
		extent.flush();
		extent.close();
	}
	
	public static void intialization() {
		String browser = prop.getProperty("browser");
		if(browser.equalsIgnoreCase("chrome")) {
			System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+ "//lib//chromedriver");	

			/*//******** For Headless Browser************
			ChromeOptions option = new ChromeOptions();
			option.addArguments("window-size=1400,800");// mandatory step without this line, 
			//those websites which are not compatible with mobile version will not work.
			option.addArguments("headless");
			driver = new ChromeDriver(option);
			//************** headless browser code ends here ***********
			 */    

			/*		// For disabling the images of a webpage
			ChromeOptions options = new ChromeOptions();
			HashMap<String,Object> images = new HashMap<String,Object>();
			images.put("images", 2);
			HashMap<String,Object> prefs = new HashMap<String,Object>();
			prefs.put("profile.default_content_setting_values",images);
			options.setExperimentalOption("prefs", prefs);
			driver= new ChromeDriver(options);
			 */			
			driver = new ChromeDriver();
		}
		else {
			System.setProperty("webdriver.geeko.driver",System.getProperty("user.dir")+ "//lib//geekodriver");	
			driver = new FirefoxDriver();
		}

		e_driver = new EventFiringWebDriver(driver);
		// Now create object of EventListerHandler to register it with EventFiringWebDriver
		eventListener = new WebEventListener();
		e_driver.register(eventListener);
		driver=e_driver;
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().pageLoadTimeout(Utilities.PAGE_LOAD_TIMEOUT,TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(Utilities.IMPLICIT_TIMEOUT,TimeUnit.SECONDS);
		driver.get(prop.getProperty("url"));
	}
	
	@AfterMethod
	public void tearDown(ITestResult result) throws IOException{
		if(result.getStatus()==ITestResult.FAILURE){
			extentTest.log(LogStatus.FAIL, "TEST CASE FAILED IS "+result.getName()); //to add name in extent report
			extentTest.log(LogStatus.FAIL, "TEST CASE FAILED IS "+result.getThrowable()); //to add error/exception in extent report
			String screenshotPath = Utilities.getScreenshot(driver, result.getName());
			extentTest.log(LogStatus.FAIL, extentTest.addScreenCapture(screenshotPath)); //to add screenshot in extent report
			//extentTest.log(LogStatus.FAIL, extentTest.addScreencast(screenshotPath)); //to add screencast/video in extent report
		}
		else if(result.getStatus()==ITestResult.SKIP){
			extentTest.log(LogStatus.SKIP, "Test Case SKIPPED IS " + result.getName());
		}
		else if(result.getStatus()==ITestResult.SUCCESS){
			extentTest.log(LogStatus.PASS, "Test Case PASSED IS " + result.getName());
		}
		extent.endTest(extentTest);
		driver.quit();
	}
}
