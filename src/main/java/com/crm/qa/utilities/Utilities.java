package com.crm.qa.utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import org.apache.commons.io.FileUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.crm.qa.base.TestBase;

public class Utilities extends TestBase{
	
	public static long PAGE_LOAD_TIMEOUT = 20;
	public static long IMPLICIT_TIMEOUT =10;
	public static String TESTDATA_SHEET_PATH = "/Users/priyankamehra/eclipse-workspace/CRMTestNG/"
			+ "src/main/java/com/crm/qa/testdata/Testdata for CRM contacts.xls";	
	static Workbook book;
	static Sheet sheet;
		
	public void switchToFrame() {
		driver.switchTo().frame("downloadFrame");
	}
	
	public static Object[][] getTestData(String sheetName)  {
		FileInputStream file = null;
		try {
			file = new FileInputStream(TESTDATA_SHEET_PATH);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		try {
			book = WorkbookFactory.create(file);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InvalidFormatException e) {
			e.printStackTrace();
		}
		sheet = book.getSheet(sheetName);
		Object[][] data = new Object[sheet.getLastRowNum()][sheet.getRow(0).getLastCellNum()];
		for (int i = 0; i < sheet.getLastRowNum(); i++) {
			for (int k = 0; k < sheet.getRow(0).getLastCellNum(); k++) {
				data[i][k] = sheet.getRow(i + 1).getCell(k).toString();
			}
		}
		return data;
	}
	// this is for WebDriver Event Listener on exception
	public static void takeScreenshotAtEndOfTest() throws IOException {
		File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		String currentDir = System.getProperty("user.dir");	
		FileUtils.copyFile(scrFile, new File(currentDir + "/screenshots/" + System.currentTimeMillis() + ".png"));		
		}
	
	// This is for extent report attachment for failed testcases
	
	public static String getScreenshot(WebDriver driver, String screenshotName) throws IOException{
		String dateName = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
		File source = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		// after execution, you can see a folder "FailedTestsScreenshots"
		// under proj folder
		String destination = System.getProperty("user.dir") + "/FailedTestsScreenshots/" + screenshotName + dateName
				+ ".png";
		FileUtils.copyFile(source, new File(destination));
		return destination;
	}
	
	// This is for screenshot of failed testcases using testNG
	public static void failedTestcasesScreenshot(String result) throws IOException {
		File  source =((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(source, new File(System.getProperty("user.dir") +"/failedTestCasesScreenshots/"
		+ result +".png"));
	}
	
	
	// This is for explicitly wait for sendkeys()
	public static void sendKeys(WebDriver driver,WebElement element,int timeout,String value) {
		new WebDriverWait(driver,timeout).
		until(ExpectedConditions.visibilityOf(element));
		element.sendKeys(value);	
	}
	

	// This is for explicitly wait for click()
	public static void clickOn(WebDriver driver,WebElement element,int timeout) {
		new WebDriverWait(driver,timeout).
		until(ExpectedConditions.elementToBeClickable(element));
		element.click();
	}
	
	// for disabling the images on the webpage while loadings for chrome
	public static void disableImageChrome(ChromeOptions options) {
		HashMap<String,Object> images = new HashMap<String,Object>();
		images.put("images", 2);
		HashMap<String,Object> prefs = new HashMap<String,Object>();
		prefs.put("profile.default_content_setting_values",images);
		
		options.setExperimentalOption("prefs", prefs);				
	}
	
	// for disabling the images on the webpage while loadings for firefox
	public static void disableImageFireFox(FirefoxOptions options) {
	FirefoxProfile profile = new FirefoxProfile();
	profile.setPreference("permissions.default.image",2);
	options.setProfile(profile);
	options.setCapability(FirefoxDriver.PROFILE,profile);
	}
	
}
